package demo.groovy.day1.collections

def m = [name: 'student', language: 'Groovy']

println m.getAt('name')     // --> student
println m['name']           // --> student
println m.name              // --> student
println m."name"            // --> student
println m.get('name')       // --> student
println m.get('language', 'Java') // --> Groovy
println m.get('expression') // --> null
println m.get('expression', 'rocks') // --> rocks
println m.get('expression') // --> rocks
println m  // --> [name: student, language: Groovy, expression: rocks] ==

// notice: m.class will be a call to the map, not to the class
// thus here we use the full getClass()
println m.getClass().name  // --> 'java.util.LinkedHashMap'

def key = 100  // Variabel som bruges som key.

m = [
   user: 'string key',
   (new Date(109, 11, 1)): 'date key',
   (-42): 'negative number key',
   (false): 'boolean key',
   (key): 'variable key'
]
m.(true) = 'boolean key'  // Key will be a string
m.(2 + 2) = 'number key'
m[(key + 1)] = 'number key'  // Key er beregnet.

println m[new Date(109, 11, 1)] // --> 'date key'
println m.get(-42) // --> 'negative number key'
println m[(false)] // --> 'boolean key'
println m[100] // --> 'variable key'
println m.getAt(key) // --> 'variable key'
println m['true'] // --> 'boolean key', Key is a string
println m.'4' // --> 'number key'
println m.get(101) // --> 'number key'


