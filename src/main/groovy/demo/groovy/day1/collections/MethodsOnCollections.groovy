package demo.groovy.day1.collections

import demo.groovy.day1.Person

def list = [
        new Person(firstName: "Steve", lastName: "Johnson", age: 42),
        new Person(firstName: "Eric", lastName: "Peterson", age: 40),
        new Person(firstName: "Eve", lastName: "Andersen", age: 11),
        new Person(firstName: "Douglas", lastName: "Andersen", age: 75),
        new Person(firstName: "Amy", lastName: "Johnson", age: 15),
        new Person(firstName: "Joe", lastName: "Peterson", age: 55),
        new Person(firstName: "Jack", lastName: "Peterson", age: 49),
        new Person(firstName: "Steve", lastName: "Peterson", age: 32),
]

