package demo.groovy.day1.gdk.answer

def root = new File('/Users/sbg/projects/kursus/kursus_2014_03/groovy-grails-kursus/groovy-day-2')

//root.eachDirRecurse { dir ->
//    dir.eachFileMatch(FileType.FILES, ~/.*\.groovy/) {
//        println it.absolutePath - root.absolutePath
//    }
//}

def output = new File('/Users/sbg/projects/kursus/kursus_2014_03/groovy-grails-kursus/GroovyAndGrailsDay1_copy2.pdf')
def input = new File('/Users/sbg/projects/kursus/kursus_2014_03/groovy-grails-kursus/GroovyAndGrailsDay1.pdf')

output.withOutputStream { outputStream ->
    input.withInputStream { inputStream ->
        outputStream << inputStream
    }
}

