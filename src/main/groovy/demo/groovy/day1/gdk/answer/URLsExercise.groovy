package demo.groovy.day1.gdk.answer

def url = "http://geo.oiorest.dk/regioner.xml".toURL()

// First solution
url.eachLine {
    if(it.contains('<nr>')) {
        println "Region number: ${it - '<nr>' - '</nr>'}"
    }
    if(it.contains('<navn>')) {
        println "Region name  : ${it - '<navn>' - '</navn>'}"
    }
}

// Second solution
def nr = ~/\s+<nr>(.*)<\/nr>/
def navn = ~/\s+<navn>(.*)<\/navn>/

url.eachLine {
    if(it ==~ nr) {
        println "Region number: ${(it =~ nr)[0][1]}"
    }
    if(it ==~ navn) {
        println "Region name:   ${(it =~ navn)[0][1]}"
    }
}

// Third solution
def regex = ~/\s+<(nr|navn)>(.*)<\/\w+>/
def map = [nr: "Region number:",navn: "Region name  :"]
url.readLines().grep(regex).each {
    def matcher = it =~ regex
    println "${map[matcher[0][1]]} ${matcher[0][2]}"
}

