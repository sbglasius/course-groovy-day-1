package demo.groovy.day1.intro

import demo.java.day1.Person

def me = new Person(firstName: "Soren", lastName:
        "Glasius", age: 43)

def miniMe = new Person()
miniMe.setFirstName("Peter")
miniMe.setLastName("Glasius")
miniMe.setAge(15);

println me
println miniMe
