package demo.groovy.day1.intro

class HelloWorld {
    String[] names

    String toString() {
        "Hello ${names.join(', ')}"
    }

    static main(def args) {
        def helloWorld = new HelloWorld(names: ["Morten", "Marianne", "Donald", "Frank", "Peter"])
        println helloWorld
    }
}
