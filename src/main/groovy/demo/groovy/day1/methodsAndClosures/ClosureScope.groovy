package demo.groovy.day1.methodsAndClosures

class MyOtherClass {
    def delegate = ""
    def c = { y ->
        println this
        println owner
        println delegate
        println maximumNumberOfParameters
        println x * y
    }
}

def myOtherClass = new MyOtherClass()



class MyClass {
    def x = 4

    def execute(Closure c) {
        c.delegate = this
        c.resolveStrategy = Closure.DELEGATE_ONLY
        c(2)
    }
}

new MyClass().execute(myOtherClass.c)

