package demo.groovy.day1.methodsAndClosures

def myClosure = { it ->
    println it
}

myClosure("Hello world")
myClosure.call("Hello world")

//myClosure()
//myClosure("Hello", "World")

def myClosure2 = { a, b = "Copenhagen" ->
    println "$a $b"
}

//myClosure()
//myClosure2("Hi")
//myClosure2("Welcome", "here")
