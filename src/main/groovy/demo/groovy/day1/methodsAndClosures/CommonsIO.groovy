package demo.groovy.day1.methodsAndClosures

import org.apache.commons.io.FileUtils

def isFileNewerThanNow = FileUtils.&isFileNewer.rcurry(new Date())
def readFilToUTF8String = FileUtils.&readFileToString.rcurry("UTF-8")

