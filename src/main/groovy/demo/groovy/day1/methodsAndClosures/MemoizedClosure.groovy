package demo.groovy.day1.methodsAndClosures


def power = { a ->
    println "-->"
    return a * a
}.memoize()


println power(10)

println power(10)
println power(11)
