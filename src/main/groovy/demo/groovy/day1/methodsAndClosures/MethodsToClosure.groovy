package demo.groovy.day1.methodsAndClosures

public class SomeJavaClass {
    public static String sayHello(String text) {
        return "Static method says hello to " + text;
    }

    public String sayHi(String text) {
        return "Method says hi to " + text;
    }
}
def sayHelloStatic = SomeJavaClass.&sayHello
def sayHi = new SomeJavaClass().&sayHi

println sayHelloStatic.call('student')//-->'Static method says hello to student'
println sayHi.call('student')         //--> 'Method says hi two student'
