package demo.groovy.day1.methodsAndClosures

abstract class Human {}
class Male extends Human {}
class Female extends Human {}

void call(Human h) {
    println("Call with Human")
}

void call(Male m) {
    println("Call with Male")
}

void call(Female f) {
    println("Call with Female")
}

Human p = new Male()
Human c = new Female()

call(p);
call(c);


