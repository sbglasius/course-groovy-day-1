package demo.groovy.day1.operators

result = []
for(int i = 0; i < 5; i++) {
    result << i
}
println result

result = []
for (int i: (0..<5)) {
    result << i
}
println result


result = []
for (i in (0..<5)) {
    result << i
}
println result

