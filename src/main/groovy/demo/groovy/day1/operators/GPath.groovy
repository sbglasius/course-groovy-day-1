package demo.groovy.day1.operators

class Order {
    Date date = new Date()
    List<OrderLine> lines
}
class OrderLine {
    String product
    BigDecimal price
    Integer count

    def getTotal() { count * price }
}

def orderLines = [
    new OrderLine(product: 'PRD1', price: 1.02, count: 10),
    new OrderLine(product: 'PRD2', price: 8.21, count: 3),
    new OrderLine(price: 10)
]
def order = new Order(lines: orderLines)

// Use GPath two traverse object graph .
println order.lines[0].product  // --> PRD1
println order.lines[1].price    // --> 8.21
println order.lines[1].total    // --> 24.63

