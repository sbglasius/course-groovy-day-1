package demo.groovy.day1.operators

Order empty = null
try {
    empty.getLines().get(0).getTotal()
} catch (npe) {
    println npe.message // --> NPE
}

def orderLines = [
    new OrderLine(product: 'PRD1', price: 1.02, count: 10),
    new OrderLine(product: 'PRD2', price: 8.21, count: 3),
]
def order = new Order(lines: orderLines)

def total = 0;
if (order != null && order.getLines() != null) {
    total = order.getLines().get(0).getTotal();
}
println total // --> 10.20

println order?.lines[1]?.total // --> 24.63

println empty?.lines?.get(0)?.total ?: 0

if(x in [1,2,3]) {

}
