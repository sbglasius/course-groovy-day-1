package demo.groovy.day1.operators

assert -1 == ('a' <=> 'b') // a < b
assert 0 == (42 <=> 42)    // a == b
assert 1 == ('b' <=> 'a')  // a > b (b < a)


