package demo.groovy.day1.operators

def list = [[2,4],[1,4],[2,3],[1,1]]

// This is the Java way of making a sort - now make it groovy with the spaceship operator
def closure = { a, b ->
    if(a[0] > b[0]) return 1
    if(a[0] < b[0]) return -1
    // a[0] == b[0]
    if(a[1] > b[1]) return 1
    if(a[1] < b[1]) return -1
    return 0
}

assert list.sort(closure) == [[1,1],[1,4],[2,3],[2,4]]

