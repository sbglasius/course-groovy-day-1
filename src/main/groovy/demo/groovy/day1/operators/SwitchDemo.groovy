package demo.groovy.day1.operators

def testSwitch(val) {
    switch (val) {// Switch on regex pattern
        case ~/^Switch.*Groovy$/:
            return 'Pattern match'
    // Switch on the class type
        case BigInteger:
            return 'Class isInstance'
    // Switch on the range
        case 60..90:
            return 'Range contains'
    // Switch on inList
        case [21, 'demo', 9.12]:
            return 'List contains'
    // Switch on object value
        case 42056:
            return 'Object equals'
    // Switch on the closure
        case { it instanceof Integer && it < 50 }:
            return 'Closure boolean'
    // Switch on the map key
        case [groovy: 'Rocks!', version: '1 .7.6 ']:
            return "Map contains key '$val'"
        default:
            return 'default'
    }
}


println testSwitch("Switch to Groovy") // --> 'Pattern matching'
println testSwitch(42G) // --> 'Class isInstance'
println testSwitch(70) // --> ' Range contains '
println testSwitch('demo') // --> 'List contains'
println testSwitch(42056) // --> 'Object equals'
println testSwitch(20) // --> 'Closure boolean'
println testSwitch('groovy') // --> "Map contains key 'groovy'"
println testSwitch('default') // --> 'Default'
