package demo.groovy.day1.operators

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode
@ToString
class X {
    Number number

    X(def number) {
        this.number = number as Integer
    }

    X plus(X s) {
        new X(number + s.number)
    }

    X minus(X s) {
        new X(number - s.number)
    }
}
