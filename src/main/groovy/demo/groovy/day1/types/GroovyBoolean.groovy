package demo.groovy.day1.types

assert true
assert !false
assert !null
assert new Object ()
assert !""
assert "Content"
assert ![]
assert ["Content"]
assert ![:]
assert [key: "Value"]
assert !0
assert 1
assert !0.0
assert 1.1

