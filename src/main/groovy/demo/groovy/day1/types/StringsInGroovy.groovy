package demo.groovy.day1.types

def doubleQuotedString = "I'm a string"

def singleQuotedString = 'I\' ma single quoted string'

def slashyString = /This is a slashy string. Oftent used for RegEx/

def multiLinedDoubleQuotedString = """\
    I'm a multi-lined string. The clever thing is,
    we can  use "double quotes " inside the string
    definition, and that line breaks are just that:
    line breaks"""

def multiLinedSingleQuotedString ='''\
    Same as double quoted multi-lined string.
    So what is the difference?'''

println doubleQuotedString.class.name           // --> java.lang.String
println singleQuotedString.class.name           // --> java.lang.String
println slashyString.class.name                 // --> java.lang.String
println multiLinedDoubleQuotedString.class.name // --> java.lang.String
println multiLinedSingleQuotedString.class.name // --> java.lang.String


def question1 = """\
    What will the type be, if I evaluate an
    expression like this: ${1+1}
"""

def question2 ='''\
    And what about this one: ${1+1}
    Is there any difference?'''

def question3 = /${1+1}/

//println question1.class.name // --> ?
//println question2.class.name // --> ?
//println question3.class.name // --> ?
