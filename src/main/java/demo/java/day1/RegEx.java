package demo.java.day1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx {
    public static void main(String[] args) {
        String text = "Java is a bit old fashioned, " +
                "where as Groovy is pretty cool. " +
                "Groovy is base of Grails and Griffon";
        Pattern pattern = Pattern.compile("((Gr[\\w]+)|(Java))");

        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            System.out.println(matcher.group());
        }
        System.out.println(matcher.matches());
    }
}
